#!/bin/sh
mkdir key
openssl genrsa -out key/key.pem 1024
openssl req -new -key key/key.pem -out key/request.pem
openssl x509 -req -in key/request.pem -signkey key/key.pem -out key/certificate.pem
openssl pkcs8 -topk8 -outform DER -in key/key.pem -inform PEM -out key/key.pk8 -nocrypt
