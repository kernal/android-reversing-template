#!/bin/sh
./apktool b ../apktool-src -o ../unsigned.apk && \
    java -jar signapk.jar key/certificate.pem key/key.pk8 ../unsigned.apk ../signed.apk && \
        adb install ../signed.apk
